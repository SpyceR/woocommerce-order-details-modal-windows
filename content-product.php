<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop, $qode_options;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
    $classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
    $classes[] = 'last';

if(isset($qode_options['woo_product_border']) && $qode_options['woo_product_border'] !== '') {
	$classes[] = 'product_with_borders';
}

if(isset($qode_options['woo_product_text_align']) && $qode_options['woo_product_text_align'] !== '') {
	$classes[] = 'text_align_'.$qode_options['woo_product_text_align'];
}
?>
<li <?php post_class( $classes ); ?>>

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
        <div class="top-product-section">

            <a class="product_list_thumb_link">
                <span class="image-wrapper">
                <?php
                    /**
                     * woocommerce_before_shop_loop_item_title hook
                     *
                     * @hooked woocommerce_show_product_loop_sale_flash - 10
                     * @hooked woocommerce_template_loop_product_thumbnail - 10
                     */
                    do_action( 'woocommerce_before_shop_loop_item_title' );
                ?>
                </span>
            </a>

			<?php do_action('qode_woocommerce_after_product_image'); ?>

        </div>

        <a href="<?php the_permalink(); ?>" class="product-category">
            <h6><?php the_title(); ?></h6>

			<?php if(isset($qode_options['woo_separator_after_title']) && $qode_options['woo_separator_after_title'] == 'yes') { ?>
				<div class="product_list_separator separator small"></div>
			<?php } ?>

            <?php
                /**
                 * woocommerce_after_shop_loop_item_title hook
                 *
                 * @hooked woocommerce_template_loop_rating - 5
                 * @hooked woocommerce_template_loop_price - 10
                 */
                do_action( 'woocommerce_after_shop_loop_item_title' );
            ?>
        </a>
		
	<!--ORDER AND DETAILS BUTTONS-->
	<div class="buttons">

		<!-- 
		height=321&amp;width=400&amp; - change 321 and 400 to values of height and width your popup window;
		You can change 'inlineId' here, but you need to change div id in the next section too.
		P.S. Id must be unique!
		Also you can change value and title of input blocks. First is the name of a button, second - title of popup window.
		For CSS use .order-button/.details-button classes.
		-->
		 
		<input alt="#TB_inline?height=321&amp;width=400&amp;inlineId=order_<?=$product->id ?>" title="Order <?php the_title(); ?> right now!" class="thickbox order-button" type="button" value="ORDER" />

		<input alt="#TB_inline?height=700&amp;width=900&amp;inlineId=details_<?=$product->id ?>" class="thickbox details-button" type="button" value="DETAILS" />  

	</div>

	<!--ORDER AND DETAILS POPUPS-->

		<!-- Change id only if you have changed it above; If you use CF7 - replace shortcode below with yours
		IMPORTANT: Add this shortcode  [_post_title] to your CF7 Message Body to display Product Name-->
	<div id="order_<?=$product->id ?>" style="display:none">
		<?php echo do_shortcode( '[contact-form-7 id="12345" title="Order"]' ); ?>
	</div>
	
		<!-- This popup shows an excerpt of product and its full description -->
	<div id="details_<?=$product->id ?>" style="display:none">
		<h2 align="center"><?php the_title(); ?></h2>
		<div class="excerpt"><?php the_excerpt(); ?></div>
		<div class="full_desc"><?php the_content(); ?></div>
	</div>
	<!--END-->

</li>