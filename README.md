# Woocommerce order and details buttons #
Simple order and description popup windows for each product based on Woocommerce+Contact Form 7+ThickBox

## HowTo: ##
1. First add this code to your functions.php file:
```php
<?php
add_action('init', 'init_theme_method');
 
function init_theme_method() {
   add_thickbox();
}
?>
```
Thickbox is a js library that comes with WordPress so you don't need to install it or something like that.

2. Insert (or replace) **content-product.php** into your theme folder (wp-content/themes/THEME_NAME/woocommerce/content-product.php).
**Мake a copy of your own content-product.php!**

3. Read comments in that file.

*If you don't want to change your own content-product, you should just copy code between*
```html
<!--ORDER AND DETAILS BUTTONS-->
```
and
```html
<!--END-->
```
and insert it in the right place (for example just before last </li>)